sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/type/String",
	"sap/ui/model/type/Float",
	"sap/m/Input",
	"sap/m/Text",
	"sap/m/CheckBox"
], function (Controller , JSONModel, StringType, Float, Input, Text, CheckBox) {
	"use strict";

	return Controller.extend("Tutoriales.ListBinding.controller.View1", {
		onInit: function () {
		
		},createContent: function (sId, oContext) {
		var oRevenue = oContext.getProperty("revenue");
			switch(typeof oRevenue) {
				case "string":
					return new Text(sId, {
						text: {
							path: "revenue",
							type: new StringType()
						}
					});
  
				case "number":
					return new Input(sId, {
						value: {
							path: "revenue",
							type: new Float()
						}
					});
				
				case "boolean":
					return new CheckBox(sId, {
						checked: {
							path: "revenue"
						}
					});
			}
		}
	});
});