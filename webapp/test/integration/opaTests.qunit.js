/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"Tutoriales/ListBinding/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});